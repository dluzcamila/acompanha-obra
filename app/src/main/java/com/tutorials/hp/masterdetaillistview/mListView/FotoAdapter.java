package com.tutorials.hp.masterdetaillistview.mListView;

/**
 * Created by Camila da Luz on 03/10/2016.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tutorials.hp.masterdetaillistview.R;
import com.tutorials.hp.masterdetaillistview.mData.Ocorrencia;

import java.util.List;

public class FotoAdapter extends BaseAdapter{

    private List<Ocorrencia> usrs;
    private Context context;

    public FotoAdapter(Context context, List<Ocorrencia> usrs) {
        this.usrs = usrs;
        this.context = context;
    }

    @Override
    public int getCount() {
        return usrs.size();
    }

    @Override
    public Object getItem(int arg0) {
        return usrs.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return usrs.get(arg0).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rootView = LayoutInflater.from(context).inflate(R.layout.lista_fotos, parent, false);

        ImageView ivFoto = (ImageView) rootView.findViewById(R.id.ivLvFoto);


        Ocorrencia usuarioDaVez = usrs.get(position);

        Bitmap bitmap = BitmapFactory.decodeByteArray(usuarioDaVez.getFoto(), 0, usuarioDaVez.getFoto().length);
        ivFoto.setImageBitmap(bitmap);

        return rootView;
    }


}
