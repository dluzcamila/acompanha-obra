package com.tutorials.hp.masterdetaillistview.mListView;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorials.hp.masterdetaillistview.MainActivity;
import com.tutorials.hp.masterdetaillistview.R;
import com.tutorials.hp.masterdetaillistview.mData.Ocorrencia;

import java.util.List;

public class GaleriaFotosActivity extends FragmentActivity {
    private ListView lvCliente;
    private List<Ocorrencia> clnt;
    private OcorrenciaDAO dao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.galeria);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Bundle b = getIntent().getExtras();
        Log.i("id", b.getString("id", null));
        final String teste = b.getString("id", null);


        dao = new OcorrenciaDAO();
        lvCliente = (ListView) findViewById(R.id.lvClientes);


        Button btCadastrarFoto= (Button) findViewById(R.id.btCadastrarFoto);
        btCadastrarFoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent it = new Intent(GaleriaFotosActivity.this, CadastroFotoActivity.class);
                it.putExtra("id", teste);
                startActivity(it);
            }
        });
    }

    protected void onResume() {
        super.onResume();

        dao = new OcorrenciaDAO();
        clnt = dao.BuscarTodosOcorrencias();
        if (clnt != null) {
            FotoAdapter usrAdp = new FotoAdapter(this, clnt);
            lvCliente.setAdapter(usrAdp);
        }

        else {
            Toast.makeText(GaleriaFotosActivity.this, "Erro ao conectar", Toast.LENGTH_LONG).show();
            finish();
        }
    }

}