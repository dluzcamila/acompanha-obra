package com.tutorials.hp.masterdetaillistview.mListView;

/**
 * Created by Camila da Luz on 18/10/2016.
 */
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorials.hp.masterdetaillistview.R;
import com.tutorials.hp.masterdetaillistview.mData.Ocorrencia;

public class CadastroFotoActivity extends Activity {
    private ImageView ivSelectedImage;
    private static final int REQUEST_CODE = 1889;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        setContentView(R.layout.cadastro_foto);

        Button btCadastro = (Button) findViewById(R.id.btCadastro);
        Button btSelect = (Button) findViewById(R.id.btSelect);

        ivSelectedImage = (ImageView) findViewById(R.id.ivSelectedImage);

        Bundle b = getIntent().getExtras();
        Log.i("id", b.getString("id", null));
        final String teste = b.getString("id", null);

        btSelect.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePictureIntent, 5678);
            }
        });

        btCadastro.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {


                Bitmap bitmap = ((BitmapDrawable)ivSelectedImage.getDrawable()).getBitmap();

                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();


                OcorrenciaDAO dao = new OcorrenciaDAO();
                Ocorrencia usr = new Ocorrencia();

                usr.setFoto(byteArray);
                usr.setTipo("foto");
                usr.setComentario(null);
                usr.setId_obra(Integer.parseInt(teste));

                usr = dao.InserirOcorrencia(usr);

                if (usr != null) {
                    finish();
                } else {
                    Toast.makeText(CadastroFotoActivity.this, "Erro no Cadastro", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if(requestCode == 5678 && resultCode == RESULT_OK){
            //imagem veio da camera
            Bundle extras = data.getExtras();
            Bitmap imagem = (Bitmap) extras.get("data");
            ivSelectedImage.setImageBitmap(imagem);
        }

    }
}