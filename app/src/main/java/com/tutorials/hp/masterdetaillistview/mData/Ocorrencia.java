package com.tutorials.hp.masterdetaillistview.mData;


import java.io.Serializable;

public final class Ocorrencia implements Serializable {

	private static final long serialVersionUID = -2229832341556924673L;

	private String tipo;
	private String comentario;
	private byte[] foto;
	private Integer id_obra;
	private Integer id;
	//private Date data;
	
	public Ocorrencia(){}
	
	public Ocorrencia(String tipo, String comentario, byte[]foto, Integer id_obra, Integer id) {
		this.tipo = tipo;
		this.comentario = comentario;
		this.id_obra = id_obra;
		this.foto = foto;
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	public Integer getId_obra() {
		return id_obra;
	}
	public void setId_obra(Integer id_obra) {
		this.id_obra = id_obra;
	}
	
	public byte[] getFoto() {
		return foto;
	}
	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String toString() {
		return tipo + " " + comentario + " " + foto + " "+ id_obra ;
	}
	
	
	
	
	
	

}
