package com.tutorials.hp.masterdetaillistview.mListView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tutorials.hp.masterdetaillistview.R;
import com.tutorials.hp.masterdetaillistview.mData.Obra;
import com.tutorials.hp.masterdetaillistview.mDetail.DetalheActivity;

import java.util.ArrayList;

/**
 * Created by Oclemy on 5/11/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class CustomAdapter extends BaseAdapter {

    Context c;
    ArrayList<Obra> obras;
    LayoutInflater inflater;



    public CustomAdapter(Context c, ArrayList<Obra> obras) {
        this.c = c;
        this.obras = obras;

        inflater= (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return obras.size();
    }

    @Override
    public Object getItem(int position) {
        return obras.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            convertView=inflater.inflate(R.layout.model,parent,false);
        }

        TextView nameTxt= (TextView) convertView.findViewById(R.id.nameTxt);
        ImageView img= (ImageView) convertView.findViewById(R.id.image);
        TextView custoTxt = (TextView)convertView.findViewById(R.id.custo);

        final String name=obras.get(position).getName();
        final int image=obras.get(position).getImage();
        final  Double coord2 =obras.get(position).getCoord2();
        final  Double coord1 =obras.get(position).getCoord1();
        final String custo=obras.get(position).getCusto();
        final Integer id = obras.get(position).getId();



        //BIND DATA
        nameTxt.setText(name);
        img.setImageResource(image);
        custoTxt.setText(custo);



        //ITEMCLICK
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDetailActivity(name,image,coord1, coord2, id);
            }
        });

        return convertView;
    }

    //OPEN DETAIL ACTIVITY AND PASS DATA
    private void openDetailActivity(String name, int image, Double coord1, Double coord2, Integer id)
    {
        Intent i=new Intent(c, DetalheActivity.class);
        //PACK  DATA
        i.putExtra("NAME_KEY",name);
        i.putExtra("IMAGE_KEY",image);
        i.putExtra("COORD2_KEY",coord2);
        i.putExtra("COORD1_KEY",coord1);
        i.putExtra("ID_KEY",id);



        //open activity
        c.startActivity(i);
    }
}
