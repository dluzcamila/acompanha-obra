package com.tutorials.hp.masterdetaillistview;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.tutorials.hp.masterdetaillistview.mData.ObrasCollection;
import com.tutorials.hp.masterdetaillistview.mListView.CustomAdapter;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView lv= (ListView) findViewById(R.id.lv);
        CustomAdapter adapter=new CustomAdapter(this, ObrasCollection.getObras());
        lv.setAdapter(adapter);

    }


}
