package com.tutorials.hp.masterdetaillistview.mListView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tutorials.hp.masterdetaillistview.R;
import com.tutorials.hp.masterdetaillistview.mData.Ocorrencia;

import java.io.ByteArrayOutputStream;

public class CadastroComentarioActivity extends Activity {
    private ImageView ivSelectedImage;
    private static final int REQUEST_CODE = 1889;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.cadastro);
        Bundle b = getIntent().getExtras();
        Log.i("id", b.getString("id", null));

        final String teste = b.getString("id", null);
        final EditText edComentario = (EditText) findViewById(R.id.etComentario);
        Button btCadastro = (Button) findViewById(R.id.btCadastro);


        btCadastro.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                OcorrenciaDAO dao = new OcorrenciaDAO();
                Ocorrencia usr = new Ocorrencia();

                usr.setTipo("comentario");
                usr.setComentario(edComentario.getText().toString());
                usr.setId_obra(Integer.parseInt(teste));
                usr.setFoto(null);


                usr = dao.InserirOcorrencia(usr);

                if (usr != null) {
                    finish();
                } else {
                    Toast.makeText(CadastroComentarioActivity.this, "Erro no Cadastro", Toast.LENGTH_LONG).show();
                }
            }
        });

    }


}