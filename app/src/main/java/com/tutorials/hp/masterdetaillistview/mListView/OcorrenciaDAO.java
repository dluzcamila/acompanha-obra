package com.tutorials.hp.masterdetaillistview.mListView;

import android.util.Base64;
import android.util.Log;

import com.tutorials.hp.masterdetaillistview.mData.Ocorrencia;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by Camila da Luz on 17/10/2016.
 */
public class OcorrenciaDAO {
    public static final String URL = "http://192.168.1.104:8080/ExemploWS/services/ClienteDAO?wsdl";
    private static final String NAMESPACE = "http://exemploWS.video.com.br";

    private static final String BUSCAR_TODOS= "buscarTodosOcorrencias";
    private static final String INSERIR = "InserirOcorrencia";


    public List<Ocorrencia> BuscarTodosOcorrencias() {

        List<Ocorrencia> listaOcor = new ArrayList<Ocorrencia>();

        SoapObject buscarOcorrencias = new SoapObject(NAMESPACE, BUSCAR_TODOS);


        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(buscarOcorrencias);

        envelope.implicitTypes = true;

        HttpTransportSE http = new HttpTransportSE(URL);

        try {
            http.call("urn:" + BUSCAR_TODOS, envelope);

            if (envelope.getResponse() instanceof SoapObject) {
                SoapObject resposta = (SoapObject) envelope.getResponse();

                Ocorrencia usr = new Ocorrencia();

                usr.setId(Integer.parseInt(resposta.getProperty("id").toString()));
                usr.setTipo(resposta.getProperty("tipo").toString());
                usr.setComentario(resposta.getProperty("comentario").toString());
                usr.setId_obra(Integer.parseInt(resposta.getProperty("id_obra").toString()));


                String foto = resposta.getProperty("foto").toString();

                byte[] bt = Base64.decode(foto, Base64.DEFAULT);
                usr.setFoto(bt);
                listaOcor.add(usr);
            } else {
                Vector<SoapObject> retorno = (Vector<SoapObject>) envelope.getResponse();

                for (SoapObject resposta : retorno) {

                    Ocorrencia usr = new Ocorrencia();

                    usr.setId(Integer.parseInt(resposta.getProperty("id").toString()));
                    usr.setTipo(resposta.getProperty("tipo").toString());
                    usr.setComentario(resposta.getProperty("comentario").toString());
                    usr.setId_obra(Integer.parseInt(resposta.getProperty("id").toString()));


                    String foto = resposta.getProperty("foto").toString();

                    byte[] bt = Base64.decode(foto, Base64.DEFAULT);
                    usr.setFoto(bt);
                    listaOcor.add(usr);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();

            Log.d("WEBSERVICE", "ERRO");
            return null;
        }

        return listaOcor;
    }

    public Ocorrencia InserirOcorrencia(Ocorrencia ocorrencia) {
        Log.d("teste", ocorrencia.toString());

        SoapObject inserirOcorrencia = new SoapObject(NAMESPACE, INSERIR);
        SoapObject usr = new SoapObject(NAMESPACE, "ocorrencia");
        usr.addProperty("tipo", ocorrencia.getTipo());
        usr.addProperty("comentario", ocorrencia.getComentario());
        usr.addProperty("id_obra", ocorrencia.getId_obra());
        usr.addProperty("foto", ocorrencia.getFoto());

        inserirOcorrencia.addSoapObject(usr);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.setOutputSoapObject(inserirOcorrencia);

        new MarshalBase64().register(envelope);

        envelope.implicitTypes = true;

        HttpTransportSE http = new HttpTransportSE(URL);

        try {

            http.call("urn:" + INSERIR, envelope);

            SoapPrimitive resposta = (SoapPrimitive) envelope.getResponse();
            int id = Integer.parseInt(resposta.toString());

            if (id > 0) {
                ocorrencia.setId(id);
                return ocorrencia;
            } else {
                return null;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
