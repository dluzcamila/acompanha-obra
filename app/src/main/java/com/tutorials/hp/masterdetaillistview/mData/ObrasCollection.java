package com.tutorials.hp.masterdetaillistview.mData;

import com.tutorials.hp.masterdetaillistview.R;

import java.util.ArrayList;

/**
 * Created by Oclemy on 5/11/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class ObrasCollection {

    public static ArrayList<Obra> getObras()
    {
        ArrayList<Obra> Obras=new ArrayList<>();
        Obra Obra=null;

        //ADD DATA TO COLLECTION
        Obra=new Obra();
        Obra.setName("Hospital Regional");
        Obra.setCusto("7.000.000");
        Obra.setImage(R.drawable.teste);
        Obra.setCoord1(-29.704742);
        Obra.setCoord2(-53.863426);
        Obra.setId(1);

        Obras.add(Obra);

        Obra=new Obra();
        Obra.setName("Hospital Regional");
        Obra.setCusto("7.000.000");
        Obra.setImage(R.drawable.teste);
        Obra.setCoord1(-29.704742);
        Obra.setCoord2(-53.863426);
        Obra.setId(2);

        Obras.add(Obra);

        Obra=new Obra();
        Obra.setName("Hospital Regional");
        Obra.setCusto("7.000.000");
        Obra.setImage(R.drawable.teste);
        Obra.setCoord1(-29.704742);
        Obra.setCoord2(-53.863426);
        Obra.setId(3);
        Obras.add(Obra);





        //RETURN COLLECTION
        return Obras;
    }

}
