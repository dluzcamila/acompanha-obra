package com.tutorials.hp.masterdetaillistview.mDetail;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.tutorials.hp.masterdetaillistview.MainActivity;
import com.tutorials.hp.masterdetaillistview.R;
import com.tutorials.hp.masterdetaillistview.mData.Ocorrencia;
import com.tutorials.hp.masterdetaillistview.mListView.CadastroComentarioActivity;
import com.tutorials.hp.masterdetaillistview.mListView.GaleriaFotosActivity;
import com.tutorials.hp.masterdetaillistview.mListView.OcorrenciaAdapter;
import com.tutorials.hp.masterdetaillistview.mListView.OcorrenciaDAO;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class DetalheActivity extends FragmentActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    TextView nameTxt;
    TextView aviso;
    private ListView lvCliente;

    private OcorrenciaDAO dao;
    LoginButton loginButton;
    CallbackManager callbackManager;
    Uri imageuri;
    AccessTokenTracker accessTokenTracker;
    String username, firstname, lastname;
    TextView tv_profile_name;
    ImageView iv_profile_pic;
    String teste;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_detalhe);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        tv_profile_name = (TextView) findViewById(R.id.tv_profile_name);
        iv_profile_pic = (ImageView) findViewById(R.id.iv_profile_pic);

        String teste = pref.getString("nome2", null);
        String teste2 = pref.getString("id", null);
        if(teste == null)
            Log.d("nome", "nulo");
        else if(teste.equals("sair")) {
            Log.d("nome", "saiu");
        }
        else {
            Log.d("nome", teste);
            Log.d("id", teste2);
            tv_profile_name.setText(teste);
            String imageurl = "https://graph.facebook.com/" + teste2 + "/picture?type=large";
            Picasso.with(DetalheActivity.this).load(imageurl).into(iv_profile_pic);
        }


        loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday, user_friends"));
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref.edit();


                                try {
                                    String email = object.getString("email");
                                    String birthday = object.getString("birthday");
                                    String id = object.getString("id");
                                    String name = object.getString("name");
                                    tv_profile_name.setText(name);

                                    editor.putString("nome2", name);
                                    editor.putString("id", id);
                                    // Saving string
                                    editor.commit();
                                    // commit changes



                                    String imageurl = "https://graph.facebook.com/" + id + "/picture?type=large";

                                    Picasso.with(DetalheActivity.this).load(imageurl).into(iv_profile_pic);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        });


                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday");
                request.setParameters(parameters);
                request.executeAsync();


/**
 * AccessTokenTracker to manage logout
 */
                accessTokenTracker = new AccessTokenTracker() {
                    @Override
                    protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken,
                                                               AccessToken currentAccessToken) {
                        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        if (currentAccessToken == null) {

                            editor.remove("nome2"); // will delete key key_nam
                            editor.commit();
                            editor.putString("nome2","sair");  // Saving string
                            editor.commit();
                            tv_profile_name.setText("");
                            iv_profile_pic.setImageResource(R.drawable.usuario);

                        }
                    }
                };
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
        nameTxt= (TextView) findViewById(R.id.nameTxtDetail);

        Intent i=this.getIntent();
        String name=i.getExtras().getString("NAME_KEY");
        final Integer id = i.getExtras().getInt("ID_KEY");

        nameTxt.setText(name);


        dao = new OcorrenciaDAO();
        Button btCadastrar= (Button) findViewById(R.id.btCadastrar);
        Button btGaleria= (Button) findViewById(R.id.btGaleria);

        lvCliente = (ListView) findViewById(R.id.lvClientes);

        btCadastrar.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent it = new Intent(DetalheActivity.this, CadastroComentarioActivity.class);
                it.putExtra("id", id.toString());

                startActivity(it);
            }
        });

        btGaleria.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent it = new Intent(DetalheActivity.this, GaleriaFotosActivity.class);
                it.putExtra("id", id.toString());

                startActivity(it);
            }
        });

    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    protected void onResume() {
        super.onResume();

        List<Ocorrencia> clnt = null;
        aviso = (TextView) findViewById(R.id.aviso);
        aviso.setText("");
        dao = new OcorrenciaDAO();
        clnt = dao.BuscarTodosOcorrencias();
        if (clnt != null) {
            OcorrenciaAdapter usrAdp = new OcorrenciaAdapter(this, clnt);
            lvCliente.setAdapter(usrAdp);


        }
        else{aviso.setText("Sem conexão!");}
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Intent i=this.getIntent();
        double coord1 = i.getExtras().getDouble("COORD1_KEY");
        double coord2 = i.getExtras().getDouble("COORD2_KEY");
        mMap = googleMap;

        LatLng hr = new LatLng(coord1, coord2);
        mMap.addMarker(new MarkerOptions().position(hr).title("Hospital Regional - SM"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hr, 12));

    }


}
