package com.tutorials.hp.masterdetaillistview.mData;

/**
 * Created by Oclemy on 5/11/2016 for ProgrammingWizards Channel and http://www.camposha.com.
 */
public class Obra {

    String name;
    String custo;
    String prazo;
    String endereco;
    String inicio;
    String previsao;
    Double coord1;
    Double coord2;
    Integer id;

    int image;

    public Obra() {
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getCusto() {
        return custo;
    }
    public void setCusto(String custo) {this.custo = custo;}

    public Double getCoord2() {
        return coord2;
    }
    public void setCoord2(Double coord2) {
        this.coord2 = coord2;
    }

    public Double getCoord1() {
        return coord1;
    }
    public void setCoord1(Double coord1) {
        this.coord1 = coord1;
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrevisao() {return previsao;}
    public void setPrevisao(String previsao) {
        this.previsao = previsao;
    }

    public String getInicio() {
        return inicio;
    }
    public void setInicio(String inicio) {
        this.inicio = inicio;
    }

    public int getImage() {
        return image;
    }
    public void setImage(int image) {
        this.image = image;
    }

    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getPrazo() {
        return prazo;
    }
    public void setPrazo(String prazo) {
        this.prazo = prazo;
    }
}
